package markupcode.schedulemobile.api

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import markupcode.schedulemobile.models.Group
import markupcode.schedulemobile.models.Schedule
import markupcode.schedulemobile.serializators.fromJson

class GroupScheduleApi(endpoints: EndpointStorage) : Api(endpoints) {

    fun get(group: Group): Schedule? {
        var schedule: Schedule? = null

        if (group.id == null)
            return schedule

        getUri(Endpoint.GroupSchedules).httpGet(listOf(Pair("id", group.id))).response { _, _, result ->
            when (result) {
                is Result.Success -> {
                    schedule = String(result.value).fromJson()
                }
            }
        }

        return schedule
    }
}
package markupcode.schedulemobile.api

abstract class Api(protected val endpoints: EndpointStorage) {

    protected fun getUri(endpoint: Endpoint): String {
        return "${endpoints.get(Endpoint.Base)}${endpoints.get(endpoint)}"
    }
}
package markupcode.schedulemobile.api

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import markupcode.schedulemobile.models.Group
import markupcode.schedulemobile.serializators.fromJson

class GroupApi(endpoints: EndpointStorage) : Api(endpoints) {

    fun getAll(): List<Group> {
        var groups: List<Group> = listOf()

        getUri(Endpoint.Groups).httpGet().response { _, _, result ->
            when (result) {
                is Result.Success -> {
                    groups = String(result.value).fromJson()
                }
            }
        }

        return groups
    }

    fun get(group: Group): Group? {
        var finded: Group? = null

        if (group.id == null)
            return finded

        getUri(Endpoint.Groups).httpGet(listOf(Pair("id", group.id))).response { _, _, result ->
            when (result) {
                is Result.Success -> {
                    finded = String(result.value).fromJson()
                }
            }
        }

        return finded
    }
}
package markupcode.schedulemobile.api

enum class Endpoint {
    Base,
    Groups,
    Tutors,
    GroupSchedules,
    TutorSchedules
}
package markupcode.schedulemobile.api

class EndpointStorage {
    protected val endpoints: Map<Endpoint, String> = mapOf(
            Endpoint.Base to "",
            Endpoint.Groups to "groups",
            Endpoint.Tutors to "tutors",
            Endpoint.GroupSchedules to "groups/schedule",
            Endpoint.TutorSchedules to "tutors/schedule"
    )

    fun get(key: Endpoint): String {
        val value = endpoints[key]

        if (value == null)
            throw IllegalArgumentException()

        return value
    }
}
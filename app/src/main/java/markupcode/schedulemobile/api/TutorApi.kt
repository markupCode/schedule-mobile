package markupcode.schedulemobile.api

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import markupcode.schedulemobile.models.Tutor
import markupcode.schedulemobile.serializators.fromJson

class TutorApi(endpoints: EndpointStorage) : Api(endpoints) {
    
    fun getAll(): List<Tutor> {
        var tutors: List<Tutor> = listOf()

        getUri(Endpoint.Tutors).httpGet().response { _, _, result ->
            when (result) {
                is Result.Success -> {
                    tutors = String(result.value).fromJson()
                }
            }
        }

        return tutors
    }

    fun get(tutor: Tutor): Tutor? {
        var finded: Tutor? = null

        if (tutor.id == null)
            return finded

        getUri(Endpoint.Tutors).httpGet(listOf(Pair("id", tutor.id))).response { _, _, result ->
            when (result) {
                is Result.Success -> {
                    finded = String(result.value).fromJson()
                }
            }
        }

        return finded
    }
}
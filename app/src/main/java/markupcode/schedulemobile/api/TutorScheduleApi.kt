package markupcode.schedulemobile.api

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import markupcode.schedulemobile.models.Schedule
import markupcode.schedulemobile.models.Tutor
import markupcode.schedulemobile.serializators.fromJson

class TutorScheduleApi(endpoints: EndpointStorage) : Api(endpoints) {

    fun get(tutor: Tutor): Schedule? {
        var schedule: Schedule? = null

        if (tutor.id == null)
            return schedule

        getUri(Endpoint.TutorSchedules).httpGet(listOf(Pair("id", tutor.id))).response { _, _, result ->
            when (result) {
                is Result.Success -> {
                    schedule = String(result.value).fromJson()
                }
            }
        }

        return schedule
    }
}
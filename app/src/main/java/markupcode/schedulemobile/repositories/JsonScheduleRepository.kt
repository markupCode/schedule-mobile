package markupcode.schedulemobile.repositories

import markupcode.schedulemobile.configs.Options
import markupcode.schedulemobile.libs.FileSystem
import markupcode.schedulemobile.libs.JsonNameGenerator
import markupcode.schedulemobile.models.Schedule
import markupcode.schedulemobile.serializators.fromJson

class JsonScheduleRepository(fileSystem: FileSystem, dirs: Options) : FileRepository<String, Schedule>(fileSystem) {
    override val generator = JsonNameGenerator()
    override val root = dirs.get("schedules")

    override fun deserialize(data: String): Schedule {
        return data.fromJson()
    }
}
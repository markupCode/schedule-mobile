package markupcode.schedulemobile.repositories

import markupcode.schedulemobile.configs.Options
import markupcode.schedulemobile.libs.FileSystem
import markupcode.schedulemobile.models.Tutor
import markupcode.schedulemobile.serializators.fromJson

class AvailableTutorDataSet(fileSystem: FileSystem, files: Options) : FileDataSet<Tutor>(fileSystem) {
    override var root: String = String()
    override var name: String = files.get("availableTutors")

    init {
        initialize()
    }

    override fun deserialize(value: String): MutableMap<String, Tutor>? {
        return value.fromJson()
    }
}
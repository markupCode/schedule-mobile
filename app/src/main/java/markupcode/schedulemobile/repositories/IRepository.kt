package markupcode.schedulemobile.repositories

interface IRepository<K: Any, V: Any> {
    fun add(key: K, value: V): Unit
    fun get(key: K): V?
    fun remove(key: K): Unit
    fun removeAll(): Unit
}
package markupcode.schedulemobile.repositories

import java.io.FileNotFoundException

import markupcode.schedulemobile.libs.FileSystem
import markupcode.schedulemobile.libs.FileNameGenerator
import markupcode.schedulemobile.serializators.json

abstract class FileRepository<K, V>(protected val fileSystem: FileSystem) : IRepository<K, V>  where K: Any, V: Any {
    protected abstract val generator: FileNameGenerator
    protected abstract val root: String

    override fun add(key: K, value: V) {
        fileSystem.saveFile(root, generator.generate(key), serialize(value))
    }

    override fun get(key: K): V? {
        try {
            val data = fileSystem.loadFile(root, buildName(key))
            return deserialize(data)
        } catch (error: FileNotFoundException) {
            return null
        }
    }

    override fun remove(key: K) {
        fileSystem.deleteFile(root, buildName(key))
    }

    override fun removeAll() {
        fileSystem.deleteAllFiles(root)
    }

    protected fun buildName(value: K): String {
        return generator.generate(value)
    }

    protected fun serialize(data: Any) : String {
        return data.json()
    }
    protected abstract fun deserialize(data: String) : V
}
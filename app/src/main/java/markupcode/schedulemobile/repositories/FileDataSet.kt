package markupcode.schedulemobile.repositories

import markupcode.schedulemobile.libs.FileSystem
import markupcode.schedulemobile.models.IModel
import markupcode.schedulemobile.serializators.ISerializable
import markupcode.schedulemobile.serializators.json
import java.io.FileNotFoundException

abstract class FileDataSet<V : IModel>(protected val fileSystem: FileSystem) {
    protected lateinit var items: MutableMap<String, V>
    protected abstract var root: String
    protected abstract var name: String

    fun initialize(): Unit {
        items = load()
    }

    fun add(value: V): Unit {
        items[value.getId()] = value
        save()
    }

    fun addAll(values: Map<String, V>): Unit {
        items.putAll(values)
        save()
    }

    fun addAll(values: List<V>): Unit {
        items.putAll(values.map { Pair(it.getId(), it) })
        save()
    }

    fun remove(value: V): Unit {
        items.remove(value.getId())
        save()
    }

    fun removeAll(): Unit {
        items.clear()
        save()
    }

    fun get(value: V): V? {
        return items[value.getId()]
    }

    fun getAll(): List<V> {
        return items.values.toList()
    }

    fun filter(predicate: (Map.Entry<String, V>) -> Boolean): Map<String, V> {
        return items.filter(predicate)
    }

    fun save(): Unit {
        val data = items.json()
        fileSystem.saveFile(root, name, data)
    }

    protected fun load(): MutableMap<String, V> {
        val data: String

        try {
            data = fileSystem.loadFile(root, name)
        } catch (error: FileNotFoundException) {
            fileSystem.createFile(root, name)
            return mutableMapOf()
        }

        val value = deserialize(data)

        if (value != null)
            return value
        return mutableMapOf()
    }

    protected abstract fun deserialize(value: String): MutableMap<String, V>?
}
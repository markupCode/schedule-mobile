package markupcode.schedulemobile.repositories

import markupcode.schedulemobile.configs.Options
import markupcode.schedulemobile.libs.FileSystem
import markupcode.schedulemobile.models.Tutor
import markupcode.schedulemobile.serializators.fromJson

class CurrentTutorData(fileSystem: FileSystem, files: Options) : FileData<Tutor>(fileSystem) {
    override val root: String = ""
    override val name: String = files.get("currentTutor")

    init {
        initialize()
    }

    override fun deserialize(value: String): Tutor? {
        return value.fromJson()
    }

}
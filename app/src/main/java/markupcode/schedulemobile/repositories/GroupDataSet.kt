package markupcode.schedulemobile.repositories

import markupcode.schedulemobile.configs.Options
import markupcode.schedulemobile.libs.FileSystem
import markupcode.schedulemobile.models.Group
import markupcode.schedulemobile.serializators.fromJson


class GroupDataSet(fileSystem: FileSystem, files: Options) : FileDataSet<Group>(fileSystem) {
    override var root: String = String()
    override var name: String = files.get("groups")

    init {
        initialize()
    }

    override fun deserialize(value: String): MutableMap<String, Group>? {
        return value.fromJson()
    }
}
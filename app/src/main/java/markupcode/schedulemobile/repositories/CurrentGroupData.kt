package markupcode.schedulemobile.repositories

import markupcode.schedulemobile.configs.Options
import markupcode.schedulemobile.libs.FileSystem
import markupcode.schedulemobile.models.Group
import markupcode.schedulemobile.serializators.fromJson

class CurrentGroupData(fileSystem: FileSystem, files: Options) : FileData<Group>(fileSystem) {
    override val root: String = ""
    override val name: String = files.get("currentGroup")

    init {
        initialize()
    }

    override fun deserialize(value: String): Group? {
        return value.fromJson()
    }

}
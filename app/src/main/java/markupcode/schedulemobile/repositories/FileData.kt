package markupcode.schedulemobile.repositories

import markupcode.schedulemobile.libs.FileSystem
import markupcode.schedulemobile.serializators.json
import java.io.FileNotFoundException

abstract class FileData<V : Any>(protected val fileSystem: FileSystem) {
    var item: V? = null
        set(value) {
            field = value
            save()
        }

    protected abstract val root: String
    protected abstract val name: String

    init {
        initialize()
    }

    fun initialize(): Unit {
        item = load()
    }

    fun save(): Unit {
        val data = if (item != null)
            serialize(item!!)
        else "{}"

        fileSystem.saveFile(root, name, data)
    }

    protected fun load(): V? {
        val data: String

        try {
            data = fileSystem.loadFile(root, name)
        } catch (error: FileNotFoundException) {
            fileSystem.createFile(root, name)
            return null
        }

        return deserialize(data)
    }

    protected fun serialize(value: Any): String {
        return value.json()
    }

    protected abstract fun deserialize(value: String): V?
}
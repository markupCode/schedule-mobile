package markupcode.schedulemobile.libs

interface IObserver {
    fun update(): Unit
}
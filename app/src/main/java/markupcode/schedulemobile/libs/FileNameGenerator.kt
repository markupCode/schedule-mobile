package markupcode.schedulemobile.libs

open class FileNameGenerator(extension: String) : IGeneratable<String> {
    protected val ext = extension
    protected val separator = "."
    protected val regex = Regex("""[\w\d]+""")

    override fun generate(value: Any): String {
        val name = build(regex.findAll(value.toString()))

        return "$name$separator$ext"
    }

    protected fun build(parts: Sequence<MatchResult>): String {
        val builder = StringBuilder()

        for (match in parts)
            builder.append(match.value)

        return builder.toString()
    }
}
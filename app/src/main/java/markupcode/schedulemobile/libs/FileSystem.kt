package markupcode.schedulemobile.libs

import android.content.Context
import android.content.Context.MODE_PRIVATE
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class FileSystem(context: Context) {

    protected val ctx: Context = context

    fun createFile(path: String, name: String): Unit {
        val dir = getDirectory(path)
        File(dir.path, name).createNewFile()
    }

    fun saveFile(path: String, name: String, data: String): Unit {
        val dir = getDirectory(path)
        FileOutputStream(File(dir.path, name)).use { it.write(data.toByteArray()) }
    }

    fun loadFile(path: String, name: String): String {
        val dir = getDirectory(path)
        return FileInputStream(File(dir.path, name)).bufferedReader().use { it.readText() }
    }

    fun deleteFile(path: String, name: String): Unit {
        val dir = getDirectory(path)
        File(dir.path, name).delete()
    }

    fun deleteAllFiles(path: String): Unit {
        val dir = getDirectory(path)

        if (!dir.isDirectory)
            throw IllegalArgumentException()

        for (file in dir.listFiles())
            file.delete()
    }

    fun getDirectory(path: String): File {
        val root = ctx.filesDir
        val dir: File

        dir = if (path.isNotEmpty()) File(root, path) else root

        if (!dir.exists())
            dir.mkdirs()

        return dir
    }
}
package markupcode.schedulemobile.libs

interface IGeneratable<T: Any>  {
    fun generate(value: Any): T
}
package markupcode.schedulemobile.libs

interface IObservable {
    fun subscribe(observer: IObserver): Unit
    fun unsubscribe(observer: IObserver): Unit
    fun notifyObservers(): Unit
}
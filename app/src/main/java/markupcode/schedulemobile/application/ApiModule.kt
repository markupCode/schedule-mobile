package markupcode.schedulemobile.application

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

import markupcode.schedulemobile.api.*

@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideEndpoints(): EndpointStorage {
        return EndpointStorage()
    }

    @Provides
    @Singleton
    fun provideGroupApi(endpoints: EndpointStorage): GroupApi {
        return GroupApi(endpoints)
    }

    @Provides
    @Singleton
    fun provideTutorApi(endpoints: EndpointStorage): TutorApi {
        return TutorApi(endpoints)
    }

    @Provides
    @Singleton
    fun provideGroupScheduleApi(endpoints: EndpointStorage): GroupScheduleApi {
        return GroupScheduleApi(endpoints)
    }

    @Provides
    @Singleton
    fun provideTutorScheduleApi(endpoints: EndpointStorage): TutorScheduleApi {
        return TutorScheduleApi(endpoints)
    }
}
package markupcode.schedulemobile.application

import javax.inject.Singleton
import android.content.Context

import dagger.Module
import dagger.Provides

import markupcode.schedulemobile.libs.FileSystem

@Module
class FileSystemModule {

    @Provides
    @Singleton
    fun provideFileSystem(context: Context): FileSystem {
        return FileSystem(context)
    }
}
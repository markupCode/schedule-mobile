package markupcode.schedulemobile.application

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

import markupcode.schedulemobile.configs.DataDirectories
import markupcode.schedulemobile.configs.DataFiles
import markupcode.schedulemobile.libs.FileSystem
import markupcode.schedulemobile.repositories.*

@Module
class RepositoriesModule {

    @Provides
    @Singleton
    fun provideScheduleRepository(fileSystem: FileSystem, dirs: DataDirectories): JsonScheduleRepository {
        return JsonScheduleRepository(fileSystem, dirs)
    }

    @Provides
    @Singleton
    fun provideGroupDataSet(fileSystem: FileSystem, files: DataFiles): GroupDataSet {
        return GroupDataSet(fileSystem, files)
    }

    @Provides
    @Singleton
    fun provideTutorDataSet(fileSystem: FileSystem, files: DataFiles): TutorDataSet {
        return TutorDataSet(fileSystem, files)
    }

    @Provides
    @Singleton
    fun provideAvailableGroupDataSet(fileSystem: FileSystem, files: DataFiles): AvailableGroupDataSet {
        return AvailableGroupDataSet(fileSystem, files)
    }

    @Provides
    @Singleton
    fun provideAvailableTutorDataSet(fileSystem: FileSystem, files: DataFiles): AvailableTutorDataSet {
        return AvailableTutorDataSet(fileSystem, files)
    }

    @Provides
    @Singleton
    fun provideCurrentGroupData(fileSystem: FileSystem, files: DataFiles): CurrentGroupData {
        return CurrentGroupData(fileSystem, files)
    }

    @Provides
    @Singleton
    fun provideCurrentTutorData(fileSystem: FileSystem, files: DataFiles): CurrentTutorData {
        return CurrentTutorData(fileSystem, files)
    }
}
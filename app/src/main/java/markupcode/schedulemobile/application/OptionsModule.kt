package markupcode.schedulemobile.application

import javax.inject.Singleton
import android.content.Context

import dagger.Module
import dagger.Provides

import markupcode.schedulemobile.configs.DataDirectories
import markupcode.schedulemobile.configs.DataFiles

@Module
class OptionsModule {

    @Provides
    @Singleton
    fun provideDataDirectories(context: Context): DataDirectories {
        return DataDirectories(context)
    }

    @Provides
    @Singleton
    fun provideDataFiles(context: Context): DataFiles {
        return DataFiles(context)
    }
}
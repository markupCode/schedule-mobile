package markupcode.schedulemobile.application

import javax.inject.Singleton

import dagger.Component

@Component(modules = [
    AppModule::class,
    FileSystemModule::class,
    OptionsModule::class,
    RepositoriesModule::class,
    ApiModule::class
])
@Singleton
interface AppComponent {

}
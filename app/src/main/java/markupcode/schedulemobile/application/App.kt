package markupcode.schedulemobile.application

import android.app.Application

class App : Application() {
    companion object {
        lateinit var component: AppComponent
    }

    init {
        component = buildComponent()
    }

    protected fun buildComponent(): AppComponent {
        return DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .fileSystemModule(FileSystemModule())
                .optionsModule(OptionsModule())
                .repositoriesModule(RepositoriesModule())
                .apiModule(ApiModule())
                .build()
    }
}
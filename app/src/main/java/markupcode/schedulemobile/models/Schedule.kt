package markupcode.schedulemobile.models

data class Schedule(
        var days: List<Day>? = listOf()
)
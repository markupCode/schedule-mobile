package markupcode.schedulemobile.models

interface IModel {
    fun getId(): String
}
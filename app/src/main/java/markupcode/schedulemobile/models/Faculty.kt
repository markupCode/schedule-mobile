package markupcode.schedulemobile.models

data class Faculty(
        var id: Int,
        var shortName: String,
        var longName: String
)
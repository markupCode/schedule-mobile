package markupcode.schedulemobile.models

data class Discipline(
        var shortName: String,
        var longName: String? = String()
)
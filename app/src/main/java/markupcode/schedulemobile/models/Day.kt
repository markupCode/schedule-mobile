package markupcode.schedulemobile.models

data class Day(var date: Date, var lessons: List<Lesson>) { }
package markupcode.schedulemobile.models

data class Group(
        var id: Int? = null,
        var name: String,
        var faculty: Faculty? = null,
        var course: Course? = null
) : IModel {
    override fun getId(): String {
        return name.toLowerCase()
    }
}
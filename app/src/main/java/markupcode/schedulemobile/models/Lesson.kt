package markupcode.schedulemobile.models

data class Lesson(
        var time: Time,
        var discipline: Discipline,
        var type: LessonType,
        var groups: List<Group> = listOf(),
        var tutors: List<Tutor> = listOf(),
        var rooms: List<Room> = listOf()
)
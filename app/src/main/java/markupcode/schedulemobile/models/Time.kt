package markupcode.schedulemobile.models

data class Time(var period: String, var number: Int) {}
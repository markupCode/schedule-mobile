package markupcode.schedulemobile.models

data class Date(
        var year: Int,
        var month: Int,
        var day: Int
)
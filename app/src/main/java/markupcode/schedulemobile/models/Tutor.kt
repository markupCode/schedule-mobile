package markupcode.schedulemobile.models

data class Tutor(
        var id: Int? = null,
        var shortName: String,
        var longName: String? = null,
        var department: Department? = null
) : IModel {
    override fun getId(): String {
        return shortName.toLowerCase()
    }
}
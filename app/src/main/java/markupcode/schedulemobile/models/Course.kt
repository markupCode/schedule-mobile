package markupcode.schedulemobile.models

data class Course(
        var id: Int,
        var name: String,
        var number: Int
)
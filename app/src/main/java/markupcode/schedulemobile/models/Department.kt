package markupcode.schedulemobile.models

data class Department(
        var id: Int,
        var shortName: String,
        var longName: String,
        var faculty: Faculty? = null
)
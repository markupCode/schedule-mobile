package markupcode.schedulemobile.serializators

interface ISerializable {
    fun serialize(value: Any): String
    fun <T> deserialize(value: String): T?
}
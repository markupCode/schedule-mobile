package markupcode.schedulemobile.configs

import android.content.Context
import android.content.Context.MODE_PRIVATE

abstract class Options(context: Context) {
    protected abstract val name: String
    protected abstract val defaultsOptions: Map<String, String>
    protected val preferences = context.getSharedPreferences(name, MODE_PRIVATE)

    init {
        defaultsOptions.forEach { if (!preferences.contains(it.key)) add(it.key, it.value) }
    }

    fun add(key: String, value: String): Unit {
        preferences.edit().putString(key, value).apply()
    }

    fun get(key: String): String {
        return preferences.getString(key, String())
    }

    fun remove(key: String): Unit {
        return preferences.edit().remove(key).apply()
    }


}


package markupcode.schedulemobile.configs

import android.content.Context

class DataDirectories(context: Context) : Options(context) {
    override val name: String
        get() = "dataDirs"
    override val defaultsOptions: Map<String, String>
        get() = mapOf<String, String>("schedules" to "schedules")
}
package markupcode.schedulemobile.configs

import android.content.Context

class DataFiles(context: Context) : Options(context) {
    override val name: String
        get() = "dataFiles"
    override val defaultsOptions: Map<String, String>
        get() = mapOf<String, String>(
                "groups" to "groups.json",
                "tutors" to "tutors.json",
                "availableGroups" to "availableGroups.json",
                "availableTutors" to "availableTutors.json",
                "currentGroup" to "currentGroup.json",
                "currentTutor" to "currentTutor.json"
        )
}
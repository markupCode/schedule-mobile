package markupcode.schedulemobile

import markupcode.schedulemobile.models.*
import markupcode.schedulemobile.serializators.fromJson
import markupcode.schedulemobile.serializators.json
import org.junit.Test
import org.junit.Assert.assertEquals

class JsonParserUnitTest {
    @Test
    fun parseList(): Unit {
        val data = listOf(1.0, 2.0, 3.0)

        val json = data.json()

        val dataFromJson = json.fromJson<List<Int>>()

        assertEquals(dataFromJson, data)
    }

    @Test
    fun parseSchedule(): Unit {
        val schedule = Schedule(listOf(
                Day(Date(2018, 5, 2), listOf(
                        Lesson(Time("08:20-09:50", 2),
                                Discipline("NotADis"),
                                LessonType("XD"))))))

        val json = schedule.json()

        val scheduleFromJson = json.fromJson<Schedule>()

        assertEquals(scheduleFromJson, schedule)
    }
}